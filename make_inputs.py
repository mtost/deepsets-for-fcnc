from coffea import processor
from coffea.analysis_tools import PackedSelection
from coffea.nanoevents.methods import vector
from hist import Hist
import awkward as ak
import numpy as np
import warnings
warnings.filterwarnings("ignore")
from math import log
import math
from itertools import chain

import tensorflow as tf

    
    
class JetWiseTraining(processor.ProcessorABC):
    
    def __init__(self, add_prob:bool = False, add_score:bool = False, no_lep:bool = False):
        self.add_prob = add_prob
        self.add_score = add_score
        self.no_lep = no_lep

    

    def make_better(self, flavor):

        if flavor==5:
            return [0,1,0,0]
        elif flavor==1 or flavor==2 or flavor==3 or flavor==4:
            return [1,0,0,0]
        else:
            return [0,0,1,0]
    

    def process(self, events):
        
        input_vector = []
        flavor_vector = []
        pb_vector = []
        pc_vector = []
        pu_vector = []
        gn2_b = []
        gn2_c = []
 
        # add cuts
        cut_lep = (ak.num(events.el) + ak.num(events.mu))==1
        cut_lep_pt_tight = ak.concatenate([events.mu.pt_NOSYS,events.el.pt_NOSYS],axis=1)[:,0]>=50e3
        cut_met_30 = events.met.met_NOSYS>=30e3
        cut_njet_3 = ak.num(events.jet) >= 3
        
        #book the cuts!
        sel_evt = PackedSelection(dtype='uint64')
        sel_evt.add("lep", cut_lep)
        sel_evt.add("lep_pt_tight", cut_lep_pt_tight)
        sel_evt.add("met_30", cut_met_30)
        sel_evt.add("njet_3", cut_njet_3)
        cuts = sel_evt.all("lep", "lep_pt_tight", "met_30", "njet_3")

        # apply the cuts
        events = events[cuts]


        # loop over events, make the vectors and append them
        for this in events:

            if not self.no_lep:
                # make the lepton information (definitely a prettier faster way to do this)
                lep_pt = this.mu.pt_NOSYS[0] if len(this.mu.pt_NOSYS) > 0 else this.el.pt_NOSYS[0]
                lep_eta = this.mu.eta[0] if len(this.mu.eta) > 0 else this.el.eta[0]
                lep_phi = this.mu.phi[0] if len(this.mu.phi) > 0 else this.el.phi[0]
                lep_e = this.mu.e_NOSYS[0] if len(this.mu.e_NOSYS) > 0 else this.el.e_NOSYS[0]

                # make the vectors one at a time, then combine
                jet_vector = [[log(jet.pt_NOSYS/1000), jet.eta, jet.phi, log(jet.e_NOSYS/1000)] for jet in this.jet]
                lep_vector = [log(lep_pt/1000), lep_eta, lep_phi, log(lep_e/1000)]

                jet_vector.append(lep_vector)

                input_vector.append(jet_vector)

                # make the flavor vector, add the lepton one at the end
                flavor_vector_jet = [self.make_better(jet.TruthFlavour) for jet in this.jet]
                flavor_vector_jet.append([0,0,0,1])
                flavor_vector.append(flavor_vector_jet)

            else:

                input_vector.append([[log(jet.pt_NOSYS/1000), jet.eta, jet.phi, log(jet.e_NOSYS/1000)] for jet in this.jet])
                flavor_vector.append([self.make_better(jet.TruthFlavour) for jet in this.jet])


            
            if self.add_prob:
                pb_vector.append([jet.DL1dv01_pb for jet in this.jet])
                pc_vector.append([jet.DL1dv01_pc for jet in this.jet])
                pu_vector.append([jet.DL1dv01_pu for jet in this.jet])

            if self.add_score:
                gn2_c.append([jet.GN2_c for jet in this.jet])
                gn2_b.append([jet.GN2_b for jet in this.jet])
            
        if not self.add_prob and not self.add_score:
            return {
                "input": input_vector,
                "flavor": flavor_vector
            }        
        
        if self.add_score and not self.add_prob:        
            return {
                "input": input_vector,
                "flavor": flavor_vector,
                "gn2_b": gn2_b,
                "gn2_c": gn2_c
            }

        if self.add_prob and not self.add_score:
            return {
                "input": input_vector,
                "flavor": flavor_vector,
                "pb": pb_vector,
                "pc": pc_vector,
                "pu": pu_vector
            }
                
        
    def postprocess(self, accumulator):
        pass





class EventWiseTraining(processor.ProcessorABC):
    
    def __init__(self, classification: str="warning!"):
        self.classification = classification
    
    def process(self, events):

        #if self.classification != 'signal' or self.classification != 'background':
        #    print(self.classification)
        #    raise Exception("Classification type not specified. Options are: 'signal' or 'background'")

        input_vector = []
        class_vector = []

        # apply the jet jvt selection
        events.jet = events.jet[((events.jet.jvt > 0.5) | (events.jet.pt > 60e3)) & (events.jet.pt > 25e3) & (abs(events.jet.eta) < 2.4)]

        # add cuts
        cut_lep = (ak.num(events.el) + ak.num(events.mu))==1
        cut_lep_pt_tight = ak.concatenate([events.mu.pt,events.el.pt],axis=1)[:,0]>=50e3       
        cut_1c = ak.sum(events.jet.DL1_c_f09>1.0, axis=1)==1
        cut_1b = ak.sum(events.jet.DL1_b>0.948, axis=1)==1
        cut_met_30 = events.met.met>=30e3
        c_jet_list = events.jet[ak.argsort(events.jet.DL1_c_f09, axis=1, ascending=False)]
        b_jet_list = events.jet[ak.argsort(events.jet.DL1_b, axis=1, ascending=False)]
        cut_b_c_overlap = ak.all(c_jet_list.pt != b_jet_list.pt, axis=1)
        
        #book the cuts!
        sel_evt = PackedSelection(dtype='uint64')
        sel_evt.add("lep", cut_lep)
        sel_evt.add("lep_pt_tight", cut_lep_pt_tight)
        sel_evt.add("1c", cut_1c)
        sel_evt.add("1b", cut_1b)
        sel_evt.add("b_c_overlap", cut_b_c_overlap)
        sel_evt.add("met_30", cut_met_30)
        cuts = sel_evt.all("lep", "lep_pt_tight", "1c", "1b", "b_c_overlap", "met_30")

        # apply the cuts
        events = events[cuts]

        #jvt selection again
        events.jet = events.jet[((events.jet.jvt > 0.5) | (events.jet.pt > 60e3)) & (events.jet.pt > 25e3) & (abs(events.jet.eta) < 2.4)]
       
        # one hot encode as follows: [jet, lep, met]
        input_vector = [list(chain(*
                       [[[log(el.pt/1000), el.eta, el.phi, log(el.e/1000), 0, 1, 0] for el in this.el],
                       [[log(mu.pt/1000), mu.eta, mu.phi, log(mu.e/1000), 0, 1, 0] for mu in this.mu], 
                       [[log(jet.pt/1000), jet.eta, jet.phi, log(jet.e/1000), 1, 0, 0] for jet in this.jet], 
                       [[0, 0, this.met.phi, log(this.met.met/1000), 0, 0, 1]]])) for this in events]


        if self.classification == 'signal':
            class_vector.extend([[0, 1]] * len(events))
        if self.classification == 'background':
            class_vector.extend([[1, 0]] * len(events))
                
        return {
            "input": input_vector,
            "class": class_vector,
        }
    
    def postprocess(self, accumulator):
        pass





class JetWiseTrainingSymmetry(processor.ProcessorABC):
    
    def __init__(self, spurions:int = 0, add_dot:bool = False, add_4vec:bool = True):
        self.spurions = spurions
        self.add_dot = add_dot
        self.add_4vec = add_4vec
    

    def make_better(self, flavor):

        if flavor==5:
            return [0,1,0]
        elif flavor==1 or flavor==2 or flavor==3 or flavor==4:
            return [1,0,0]
        else:
            return [0,0,1]

        
    def calc_dot(self, jet1, jet2):

        #convert to cartesian coordinates
        px1 = jet1.pt/1000 * math.cos(jet1.phi)
        py1 = jet1.pt/1000 * math.sin(jet1.phi)
        pz1 = jet1.pt/1000 * math.sinh(jet1.eta)

        px2 = jet2.pt/1000 * math.cos(jet2.phi)
        py2 = jet2.pt/1000 * math.sin(jet2.phi)
        pz2 = jet2.pt/1000 * math.sinh(jet2.eta)

        dot = (jet1.e/1000) * (jet2.e/1000) - (px1*px2 + py1*py2 + pz1*pz2)
        if dot > 0:
            dot = log(dot)
        else:
            dot = 0

        return dot
    

    def calc_dot_spurion(self, jets, i, j):

        # the 'i' jet is never the spurion
        e1 = jets[i].e/1000
        px1 = jets[i].pt/1000 * math.cos(jets[i].phi)
        py1 = jets[i].pt/1000 * math.sin(jets[i].phi)
        pz1 = jets[i].pt/1000 * math.sinh(jets[i].eta)

        # if j is a real jet
        if j + 1 <= len(jets):
            e2 = jets[j].e/1000
            px2 = jets[j].pt/1000 * math.cos(jets[j].phi)
            py2 = jets[j].pt/1000 * math.sin(jets[j].phi)
            pz2 = jets[j].pt/1000 * math.sinh(jets[j].eta)

        # The first spurion!
        if j + 1 - len(jets) == 1:
            e2, px2, py2, pz2, = 1, 0, 0, 1

        # The second spurion!
        if j + 1 - len(jets) == 2:
            e2, px2, py2, pz2, = 1, 0, 0, -1

        if j + 1 - len(jets) > 2:
            raise NotImplementedError("Things have gone horribly wrong!!")
        

        # now do the actual dot product stuff
        dot = e1 * e2 - (px1*px2 + py1*py2 + pz1*pz2)

        if dot > 0:
            dot = log(dot)
        else:
            dot = 0

        return dot


    def process(self, events):
        
        input_vector = []
        flavor_vector = []
 
        # apply the jet jvt selection
        events.jet = events.jet[((events.jet.jvt > 0.5) | (events.jet.pt > 60e3)) & (events.jet.pt > 25e3) & (abs(events.jet.eta) < 2.4)]

        # add cuts
        cut_lep = (ak.num(events.el) + ak.num(events.mu))==1
        cut_lep_pt_tight = ak.concatenate([events.mu.pt,events.el.pt],axis=1)[:,0]>=50e3       
        cut_1c = ak.sum(events.jet.DL1_c_f09>1.0, axis=1)==1
        cut_1b = ak.sum(events.jet.DL1_b>0.948, axis=1)==1
        cut_met_30 = events.met.met>=30e3
        c_jet_list = events.jet[ak.argsort(events.jet.DL1_c_f09, axis=1, ascending=False)]
        b_jet_list = events.jet[ak.argsort(events.jet.DL1_b, axis=1, ascending=False)]
        cut_b_c_overlap = ak.all(c_jet_list.pt != b_jet_list.pt, axis=1)
        
        #book the cuts!
        sel_evt = PackedSelection(dtype='uint64')
        sel_evt.add("lep", cut_lep)
        sel_evt.add("lep_pt_tight", cut_lep_pt_tight)
        sel_evt.add("1c", cut_1c)
        sel_evt.add("1b", cut_1b)
        sel_evt.add("b_c_overlap", cut_b_c_overlap)
        sel_evt.add("met_30", cut_met_30)
        cuts = sel_evt.all("lep", "lep_pt_tight", "1c", "1b", "b_c_overlap", "met_30")

        # apply the cuts
        events = events[cuts]

        #jvt selection again
        events.jet = events.jet[((events.jet.jvt > 0.5) | (events.jet.pt > 60e3)) & (events.jet.pt > 25e3) & (abs(events.jet.eta) < 2.4)]
        
        #reduce the overall number of jets to 8. This doesn't seem to work? Investigate
        events.jet = events.jet[:, :8]

        # loop over events, make the vectors and append them
        for this in events:
            
            if self.spurions < 1:

                if self.add_dot and self.add_4vec:
                    # this is the dot product stuff
                    jets_len = len(this.jet)
                    kinematic_stuff = [[log(jet.pt/1000), jet.eta, jet.phi, log(jet.e/1000)] for jet in this.jet]
                    dot_stuff = [[self.calc_dot(this.jet[i], this.jet[j]) for j in range(jets_len)] for i in range(jets_len)]
                    all_jets = [kinematic + dot for kinematic, dot in zip(kinematic_stuff, dot_stuff)]
                    input_vector.append([each_jet for each_jet in all_jets])
                
                elif self.add_4vec and not self.add_dot:
                    input_vector.append([[log(jet.pt/1000), jet.eta, jet.phi, log(jet.e/1000)] for jet in this.jet])

                elif self.add_dot and not self.add_4vec:
                    jets_len = len(this.jet)
                    dot_stuff = [[self.calc_dot(this.jet[i], this.jet[j]) for j in range(jets_len)] for i in range(jets_len)]
                    input_vector.append([each_jet for each_jet in dot_stuff])


            if self.spurions > 0:

                # this is where we do the spurion stuff!
                if self.add_4vec:
                    raise NotImplementedError("Spurion handling with 4-vector addition is not implemented.")
                

                jets_len = len(this.jet)

                # note we are adding two extra loops over the "j" jet!
                dot_stuff = [[self.calc_dot_spurion(this.jet, i, j) for j in range(jets_len + 2)] for i in range(jets_len)]
                input_vector.append([each_jet for each_jet in dot_stuff])



            # flavor stuff, stays the same regardless of input!
            flavor_vector.append([self.make_better(jet.truthflav) for jet in this.jet])
            
            
        return {
            "input": input_vector,
            "flavor": flavor_vector
        }        
        
                
        
    def postprocess(self, accumulator):
        pass
