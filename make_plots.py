import matplotlib.pyplot as plt
import scipy
from sklearn.metrics import roc_curve, roc_auc_score

def plot_roc(input_dir):

    plt.clf()

    for particle in input_dir.keys():
        fpr, tpr, thresholds = roc_curve(input_dir[particle]['truth'], input_dir[particle]['scores'])
        gini_score = get_gini(fpr, tpr)
        plt.plot(fpr, tpr, label=particle + " A = {}".format(gini_score))


    plt.plot([0, 1], [0, 1], 'k--', label='_nolegend_')
    plt.axis([0,1,0,1.0])
    plt.ylim(0, 1.0)

    plt.legend(loc="lower right")
    plt.xlabel('False Positive Rate') 
    plt.ylabel('True Positive Rate') 



def get_gini(x,y):
    under_curve = scipy.integrate.trapz(y,x)
    under_diagonal = 0.5
    gini = under_curve - under_diagonal
    gini = round(gini, 3)
    return gini